﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Threading;

namespace FirstAutoTestExercise
{
    [TestFixture]
    public class ShoppingCartTest
    {
        private IWebDriver driver;
        protected WebDriverWait Wait;
        private IWebElement indexPage => driver.FindElement(By.Id("page"));
         
        
        private IWebElement centerColumn => driver.FindElement(By.Id("center_column"));
        private IWebElement searchBox => indexPage.FindElement(By.Id("searchbox"));

        private IWebElement searchInput => searchBox.FindElement(By.Name("search_query"));

        private IWebElement searchButton => searchBox.FindElement(By.Name("submit_search"));
        private IWebElement SearchRoot => indexPage.FindElement(By.CssSelector("div.columns-container"));

        private IWebElement itemName => indexPage.FindElement(By.CssSelector("#best-sellers_block_right > div > ul > li:nth-child(1) > div > h5 > a"));


       private IWebElement cartForm => indexPage.FindElement(By.CssSelector("#buy_block > div > div.box-cart-bottom"));
        private IWebElement cartPar => cartForm.FindElement(By.CssSelector("#add_to_cart"));

       private  IWebElement cartButton => cartPar.FindElement(By.Name("Submit"));
         private   IWebElement cartModal => indexPage.FindElement(By.Id("layer_cart"));
        
        //private IWebElement foundItemName => indexPage.FindElement(By.CssSelector("#best-sellers_block_right > div > ul > li:nth-child(1) > div > h5 > a"));
        private IList<IWebElement> SearchedItemNames => SearchRoot.FindElements(By.CssSelector("li > div.product-container a.product-name"));

        [SetUp]
        public void SetUp()
        {
            driver = new ChromeDriver(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl("http://automationpractice.com/index.php");
        }

        [Test]
        public void SearchTest()
        {
            //Assert
            var expected = 0;
            searchInput.SendKeys("Dress");

            //Actual
            searchButton.Click();
            Wait = new WebDriverWait(driver, TimeSpan.FromSeconds(15));
            var actual = SearchedItemNames.Count;

            //Assert
            Assert.AreNotEqual(expected, actual);
        }

        [Test]
        public void AddToCartTest()
        {
            //Arrange
            var expected = true;
           
            searchInput.SendKeys("Dress");
            searchButton.Click();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            itemName.Click();
            cartButton.Click();
            Thread.Sleep(5000);

            //Actual
            var actual = cartModal.Displayed;

            //Assert
            Assert.AreEqual(expected,actual);
        }

        [TearDown]
        public void TearDown()
        {
            driver.Close();
            driver.Dispose();
        }
    }
}
